## screenshot.png

Copyright © 2018 Jakob Sinclair

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## assets/control_view/images
### down.png | left.png | left_mouse.png | middle_mouse.png | right.png | right_mouse.png | up.png | key_background.png

Public domain by qubodup

key_background.png is a modified version of left.png by Florian Kothmeier

Retrieved from [Open Game Art](https://opengameart.org/content/mouse-buttons-arrow-keys)

License: [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/)

### arrowDown.png | arrowLeft.png | arrowRight.png | arrowUp.png | buttonA.png | buttonB.png | buttonL.png | buttonL1.png | buttonL2.png | buttonR.png | buttonR1.png | buttonR2.png | buttonSelect.png | buttonStart.png | buttonX.png | buttonY.png | button1.png | button2.png | button3.png | button4.png | buttonCircle.png | buttonSquare.png | buttonTriangle.png

Public domain by Kenney

button4.png, buttonCircle.png, buttonSquare.png and buttonTriangle.png are a modified versions of button1.png by Florian Kothmeier

Retrieved from [Open Game Art](https://opengameart.org/content/game-icons)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)

## boards/controller/icons
### cake.png

Public Domain by maruki

Retrived from [Open Game Art](https://opengameart.org/content/foodies)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### cookie.png

Copyright © 2017 InkMammoth

Retrived from [Open Game Art](https://opengameart.org/content/pixel-art-food-pack-by-inkmammoth)

License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

### indicator.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### splash_background.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## boards/node
### node.escn

Copyright © Jakob Sinclair

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## boards/node/arrow
### arrow.png

Copyright © 2018 Jakob Sinclair

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## boards/node/material
### tile_blue_col.jpg | tile_red_col.png | tile_green_col.png | tile_nrm.jpg | tile_rgh.jpg

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## characters/Tux
### character.dae | tux_texture.png | tux.blend | tux.escn

Copyright © 2010 durmieu 

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### splash.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## characters/Green Tux
### character.escn | tux.escn

Copyright © 2010 durmieu 

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### tux_texture.png

Copyright © 2010 durmieu 

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### splash.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## characters/Beastie
### beastie_tex.png | beastie.blend | beastie.dae | character.escn

Copyright © 2010 durmieu 

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/beastie)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### splash.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## characters/Godette
### palette.png | godete_palette.svg | godot_chan.blend | godot_chan_one_object.blend | godot_chan.dae

Copyright © SirRichard94

Models modified by Florian Kothmeier 2018

Retrieved from [Github](https://github.com/SirRichard94/low-poly-godette)

License: [CC-BY 3.0](https://github.com/SirRichard94/low-poly-godette/blob/master/License)

### splash.png

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## fonts
### Boogaloo-Regular.ttf
Copyright (c) 2011, John Vargas Beltr�n� (www.johnvargasbeltran.com|john.vargasbeltran@gmail.com),
with Reserved Font Name Boogaloo.

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL

See fonts/OFL for full license.

## fonts/IBM-Plex-Sans/
Copyright (c) 2018, IBM
v1.1.6 Retrieved from https://github.com/IBM/plex/releases

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL

See fonts/OFL for full license.

## assets/blender_files/icon
### icon.blend

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### tux_texture.png

Copyright © 2010 durmieu 

Modified by Florian Kothmeier 2018

Retrieved from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### blue-sky-merge-clouds-675977.jpeg

Public Domain by Skitterphoto

Retrieved from [pexels.com](https://www.pexels.com/photo/air-atmosphere-blue-blue-sky-675977)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

## icons
### icon.\* | icon-\*

Copyright © 2018 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## minigames/harvest_food/fence
### fence.escn | picketfence_0.blend

Exported model of Public Domain Model by WeaponGuy

Retrieved from [OpenGameArt.org](https://opengameart.org/content/basic-wooden-fence)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

### BarrelWoodUV.jpg

Texture of fence model by WeaponGuy

Retrieved from [OpenGameArt.org](https://opengameart.org/content/basic-wooden-fence)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

## minigames/harvest_food/plants
### carrot.blend

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### carrot_rotten.blend

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## minigames/hurdle
### green_point_park_2k.hdr

Copyright © 2016 Greg Zaal

Retrived from [HDRIHaven](https://hdrihaven.com/hdri/?c=skies&h=green_point_park)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### screenshot.png

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

## minigames/hurdle/ground
### asphalt_\*

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### ground_model.escn

Copyright © Jakob Sinclair

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

## minigames/hurdle/hurdles/normal_hurdle
### hurdle_model.escn | hurdle.blend

Copyright © Jakob Sinclair

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

### plastic_\* | metal_\*

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## minigames/hurdle/hurdles/trashcan
### trashcan.escn | trashcan.blend

Copyright © yethiel

Retrieved from [Open Game Art](https://opengameart.org/content/trashcan)

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

### trashcan_diffuse.png | trashcan_spec.png | trashcan_normal.png

Public domain textures for the trashcan model by yethiel

Retrieved from [Open Game Art](https://opengameart.org/content/trashcan)

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/hurdle/powerups/landmine
### landmine.escn | landmine.blend

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/hurdle/powerups/star
### star.escn | star.blend

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/knock_off
### screenshot.png

Coyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/knock_off/ice
### ice.escn | iceberg.blend

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

### IceC_S.jpg | IceC_N.jpg

Public Domain by Keith333

Retrieved from [OpenGameArt.org](https://opengameart.org/content/snow-and-ice-batch-of-15-seamless-textures-with-normalmaps)

IceC_S.jpg modified by Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/kernel_compiling
### screenshot.png

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

## minigames/kernel_compiling/battery
### battery.blend | tex_empty.png | tex_full.png

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

## minigames/kernel_compiling/screen
### screen.blend | screen.escn

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0)

## minigames/knock_off/player_ball_material
### paper02_col.jpg | paper02_nrm.jpg | paper02_rgh.jpg

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### SnowB_S.jpg | SnowB_N.jpg

Public Domain by Keith333

Retrieved from [OpenGameArt.org](https://opengameart.org/content/snow-and-ice-batch-of-15-seamless-textures-with-normalmaps)

SnowB_S.jpg modified by Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)

## minigames/knock_off/water
### water_diffuse.png

Public Domain by Bastiaan Olij

Retrived from [Github](https://github.com/BastiaanOlij/shader_tutorial)

License: [CC0 1.0 UnBastiaan Olijiversal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## savegames
### delete_save.png

Copyright © Godot Engine

Retrieved from [github.com](https://github.com/godotengine/godot-design/blob/master/engine/icons/original/icon_remove.png)

License: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0)
